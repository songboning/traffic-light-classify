from config import Config as conf
import cv2
import json
import numpy as np
import os
import torch
from torch.utils.data import Dataset
from torch.utils.data import DataLoader


class TrainDataset(Dataset):
    def __init__(self, root=conf.dataset_dir):
        super(TrainDataset, self).__init__()
        self.root = root
        self.records = self._parse_label(json.load(open(conf.label_file)))
    
    def _parse_label(self, ori_json_data):
        train_size = int(len(ori_json_data)*conf.train_val_rate)
        label_map = {4:2, 5:1, 6:0}
        records = []
        for key, value in list(ori_json_data.items())[:train_size]:
            records.append((key, label_map[value]))
        return records
    
    def __getitem__(self, idx):
        return self.records[idx]
    
    def __len__(self):
        return len(self.records)


class ValDataset(Dataset):
    def __init__(self, root=conf.dataset_dir):
        super(ValDataset, self).__init__()
        self.root = root
        self.records = self._parse_label(json.load(open(conf.label_file)))
    
    def _parse_label(self, ori_json_data):
        train_size = int(len(ori_json_data)*conf.train_val_rate)
        label_map = {4:2, 5:1, 6:0}
        records = []
        for key, value in list(ori_json_data.items())[train_size:]:
            records.append((key, label_map[value]))
        return records
    
    def __getitem__(self, idx):
        return self.records[idx]
    
    def __len__(self):
        return len(self.records)


def read_collate(batch):
    imgs = []
    targets = []
    for filepath, target in batch:
        img = cv2.imread(os.path.join(conf.dataset_dir, filepath))
        if img is None:
            print('Load img failed', filepath)
            return None, None
        while img.shape[0] < conf.min_img_size or img.shape[1] < conf.min_img_size:
            img = cv2.resize(img, (0, 0), fx=2, fy=2)
        imgs.append(img.transpose(2, 0, 1).astype(np.float))
        targets.append(target)
    return torch.Tensor(imgs), torch.LongTensor(targets)


if __name__ == '__main__':
    trainset = TrainDataset()
    valset = ValDataset()
    train_loader = DataLoader(trainset, batch_size=conf.train_batch_size, shuffle=True, collate_fn=read_collate)
    val_loader = DataLoader(valset, batch_size=conf.test_batch_size, shuffle=True, collate_fn=read_collate)
    print(len(trainset), len(valset))
    print(next(train_loader.__iter__()))
    print('unit test end.')