from config import Config as conf
import dataset
import logging
import network
import os
import torch
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from torchvision.utils import make_grid


def train():
    if not os.path.exists(conf.output_dir):
        os.makedirs(conf.output_dir)

    device = torch.device('cuda' if torch.cuda.is_available() and conf.use_gpu else 'cpu')
    logging.info('training use device %s' % device)
    board = SummaryWriter(conf.output_dir)
    trainset = dataset.TrainDataset()
    loader = DataLoader(trainset, batch_size=conf.train_batch_size, shuffle=True, collate_fn=dataset.read_collate)
    model = network.SPPNET()

    inputs = next(iter(loader))
    board.add_graph(model, input_to_model=inputs[0])
    interval_loss = 0

    optimizer = torch.optim.Adam(model.parameters())
    model.to(device)
    model.train()

    for epoch in range(conf.epoch_num):
        for i, (imgs, targets) in enumerate(loader):
            if not isinstance(imgs, torch.Tensor):
                logging.error('loader failed')
                continue
            if imgs.shape[2] < 9 or imgs.shape[3] < 9:
                logging.warn('imgs is too small %s' % imgs.shape)
                continue

            optimizer.zero_grad()
            imgs = imgs.to(device)
            targets = targets.to(device)
            output = model(imgs)
            loss = F.cross_entropy(output, targets)
            loss.backward()
            optimizer.step()

            step = epoch * len(loader) + i + 1
            interval_loss += loss
            board.add_scalar('training_loss', loss, global_step=step)
            if (i + 1) % conf.dump_interval == 0:
                for name, value in model.named_parameters():
                    board.add_histogram(name, value, global_step=step)
                interval_loss /= conf.dump_interval
                logging.info('[epoch %2d, iter %6d /%6d] loss: %f' % (epoch, i+1, len(loader), interval_loss))
                interval_loss = 0

        torch.save(model.state_dict(), os.path.join(conf.output_dir, 'epoch%d.torchmodel'%epoch))
        logging.info('epoch %d completed, save model to %s' % (epoch, os.path.join(conf.output_dir, 'epoch%d.torchmodel'%epoch)))

        for name, value in model.named_parameters():
            if 'conv' in name and 'weight' in name:
                kernels = value.view(-1, 1, value.shape[2], value.shape[3])
                kernel_grid = make_grid(kernels, normalize=True)
                board.add_image('%s_feature_map'%name, kernel_grid, global_step=epoch)
    board.close()

    return model


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    train()