from config import Config as conf
import logging
import math
import torch
from torch import nn
from torch.nn import functional as F


class SPPNET(nn.Module):
    def __init__(self):
        super(SPPNET, self).__init__()
        
        self.conv0 = nn.Conv2d(3, conf.convs_outc[0], conf.convs_ksize[0])
        self.conv1 = nn.Conv2d(conf.convs_outc[0], conf.convs_outc[1], conf.convs_ksize[1])
        self.pool0 = nn.AdaptiveMaxPool2d(conf.spatial_pool_size[0])
        self.pool1 = nn.AdaptiveMaxPool2d(conf.spatial_pool_size[1])
        spp_out_size = sum(map(lambda x: x**2, conf.spatial_pool_size)) * conf.convs_outc[1]
        self.fc0 = nn.Linear(spp_out_size, conf.fcs_size[0])
        self.fc1 = nn.Linear(conf.fcs_size[0], conf.class_num)

        logging.info(self.__str__())
    
    def forward(self, x):
        x = self.conv0(x)
        x = self.conv1(x)
        x = [self.pool0(x), self.pool1(x)]
        x = [item.view(item.shape[0], -1) for item in x]
        x = torch.cat(x, dim=1)
        x = self.fc0(x)
        x = self.fc1(x)
        return x


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    net = SPPNET()
    print('unit test end.')