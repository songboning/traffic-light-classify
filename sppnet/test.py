from config import Config as conf
import cv2
import dataset
import logging
import network
import numpy as np
import os
from sklearn import metrics
import torch
import torch.nn.functional as F
from torch.utils.data import DataLoader
from tqdm import tqdm


def load_model(state_file, device=None):
    if device is None:
        device = torch.device('cuda' if torch.cuda.is_available() and conf.use_gpu else 'cpu')
    model = network.SPPNET()
    model.to(device)
    model.load_state_dict(torch.load(state_file, map_location=device))
    model.eval()
    logging.info('Load eval model on %s' % device)
    return model


def validation(model):
    device = next(model.parameters()).device
    valset = dataset.ValDataset()
    loader = DataLoader(valset, batch_size=conf.test_batch_size, shuffle=True, collate_fn=dataset.read_collate)
    cnt = 0
    gts = []
    dts = []

    with torch.no_grad():
        for imgs, targets in tqdm(loader):
            if not isinstance(imgs, torch.Tensor):
                logging.error('loader failed')
                continue
            if imgs.shape[2] < 9 or imgs.shape[3] < 9:
                logging.warn('imgs is too small %s' % imgs.shape)
                continue

            imgs = imgs.to(device)
            targets = targets.to(device)
            output = model(imgs)
            predict = torch.argmax(output, dim=1)
            loss = F.cross_entropy(output, targets)
            acc = (predict == targets).sum()
            gts.append(targets[0].item())
            dts.append(predict[0].item())
            cnt += 1

    report = metrics.classification_report(gts, dts, target_names=conf.class_name)
    return report


def test(model, img):
    device = next(model.parameters()).device

    while img.shape[0] < conf.min_img_size or img.shape[1] < conf.min_img_size:
        img = cv2.resize(img, (0, 0), fx=2, fy=2)
    img = img.transpose(2, 0, 1).astype(np.float)
    imgs = torch.Tensor([img])
    imgs = imgs.to(device)
    
    with torch.no_grad():
        prob = model(imgs)
        pred = torch.argmax(prob, dim=1)
    res = conf.class_name[pred[0]]
    return res


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    model = load_model(os.path.join(conf.output_dir, 'epoch0.torchmodel'))
    res = validation(model)
    print(res)
    
    valset = dataset.ValDataset()
    img = cv2.imread(os.path.join(conf.dataset_dir, valset[0][0]))
    res = test(model, img)
    print('test sample:', res)