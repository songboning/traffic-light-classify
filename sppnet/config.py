import getpass
import logging
import os


class Config:
    user = getpass.getuser()
    root_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
    model_name = os.path.split(os.path.dirname(os.path.abspath(__file__)))[1]
    output_dir = os.path.join(root_dir, 'output', user, model_name)
    
    # -------------------- data config -------------------- #
    class_num = 3
    class_name = ['red', 'yellow', 'green']
    class_names2id = dict(list(zip(class_name, list(range(class_num)))))
    dataset_dir = os.path.join(root_dir, 'data')
    label_file = os.path.join(dataset_dir, 'targets.json')
    train_val_rate = 0.5
    
    # -------------------- model config -------------------- #
    convs_outc = [96, 256]
    convs_ksize = [3, 3]
    fcs_size = [512]
    spatial_pool_size = [2, 1]
    
    # -------------------- train config -------------------- #
    # 不同尺寸的图像不能拼batch
    train_batch_size = 1
    test_batch_size = 1
    epoch_num = 1
    dump_interval = 500
    use_gpu = True
    min_img_size = 9


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    for key, value in Config.__dict__.items():
        logging.debug('config %s:\t%s' % (key, value))
    print('unit test end.')